# Study LLaVa
This repo is for studing a multimodal chat.

## How to run

```
python3.9 -m venv venv
source venv/bin/activate.fish
pip install -r requirements.txt

cd src
python llava_huggingface.py
```

# References
* [LLaVa](https://github.com/haotian-liu/LLaVA)
* [Huggingface LLaVa exmplanation](https://huggingface.co/docs/transformers/main/model_doc/llava)