from PIL import Image

import requests

from transformers import AutoProcessor, LlavaForConditionalGeneration

cache_dir='../models'
model_name = "llava-hf/llava-1.5-7b-hf"
model = LlavaForConditionalGeneration.from_pretrained(
    model_name,
    cache_dir=cache_dir
)

processor = AutoProcessor.from_pretrained(
    model_name,
    cache_dir=cache_dir
)

prompt = "<image>\nUSER: What's the content of the image?\nASSISTANT:"

url = "https://www.ilankelman.org/stopsigns/australia.jpg"

image = Image.open(requests.get(url, stream=True).raw)

inputs = processor(text=prompt, images=image, return_tensors="pt")

# Generate

generate_ids = model.generate(**inputs, max_length=30)

image = processor.batch_decode(generate_ids, skip_special_tokens=True, clean_up_tokenization_spaces=False)[0]
image.save('../data')